const { makeDirectory, writer, deleter } = require("../problem1");

makeDirectory()
  .then(() => writer())
  .then(() => deleter());
