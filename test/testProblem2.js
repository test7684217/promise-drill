const { reader, writer, nameSaver, deleter } = require("../problem2");

reader("lipsum_2.txt")
  .then((data) => {
    const UppercasedFileName = "upperCased.txt";
    return writer(UppercasedFileName, data.toUpperCase());
  })
  .then((UppercasedfileName) => {
    return nameSaver(UppercasedfileName);
  })

  .then((UppercasedfileName) => {
    return reader(UppercasedfileName);
  })

  .then((data) => {
    const LowercasedFileName = "lowerCased.txt";
    const lowerData = data.toLowerCase().split(".").join("\n");
    return writer(LowercasedFileName, lowerData);
  })
  .then((LowercasedFileName) => {
    return nameSaver(LowercasedFileName);
  })

  .then(() => {
    return reader("fileNames.txt");
  })
  .then((data) => {
    const fileNames = data.split("\n");

    let readerPromises = fileNames.map((name) => {
      if (name) {
        return reader(name);
      }
    });

    return Promise.all(readerPromises);
  })
  .then((data) => {
    const sortedData = data
      .join("\n")
      .split("\n")
      .sort()
      .filter((line) => {
        if (line.length > 0) return line;
      })
      .join();

    return writer("sortedData.txt", sortedData);
  })
  .then((sortedDataFileName) => {
    return nameSaver(sortedDataFileName);
  })
  .then(() => {
    return reader("fileNames.txt");
  })
  .then((data) => {
    const fileNames = data.split("\n");

    let deletePromises = fileNames.map((name) => {
      if (name !== "") {
        return deleter(name);
      }
    });

    return Promise.all(deletePromises);
  })
  .then(() => {
    console.log("Files deleted successfully");
  }).catch((error)=>{
    console.log(error)
  })
