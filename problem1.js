const fs = require("fs").promises;
const path = require("path");

function makeDirectory() {
  try {
    const outputDirectory = path.join(__dirname, "output");
    let makeDirectoryPromise = fs.mkdir(
      outputDirectory,
      { recursive: true },
      (err) => {
        if (err) {
          return console.log(err);
        }
      }
    );

    return makeDirectoryPromise
      .then(() => {
        console.log("Directory created");
      })
      .catch((error) => {
        console.log(error);
      });
  } catch (error) {
    return console.log(error);
  }
}

function writer() {
  try {
    let promises = [];
    for (let index = 0; index < 10; index++) {
      let newPromise = fs.writeFile(
        `output/file-${index + 1}.txt`,
        "This is a random file",
        function (err) {
          if (err) throw err;
        }
      );

      promises.push(newPromise);
    }

    return Promise.all(promises)
      .then(() => {
        console.log("Files are created successfully.");
      })
      .catch((error) => {
        console.log(error);
      });
  } catch (error) {
    return console.log(error);
  }
}

function deleter() {
  try {
    let promises = [];
    for (let index = 0; index < 10; index++) {
      let newPromise = fs.unlink(
        `output/file-${index + 1}.txt`,
        function (err) {
          if (err) throw err;
        }
      );

      promises.push(newPromise);
    }

    return Promise.all(promises)
      .then(() => {
        console.log("Files are deleted successfully.");
      })
      .catch((error) => {
        console.log(error);
      });
  } catch (error) {
    return console.log(error);
  }
}

module.exports = {
  makeDirectory,
  writer,
  deleter,
};
