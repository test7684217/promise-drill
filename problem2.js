const fs = require("fs").promises;
const path = require("path");

function reader(fileName) {
  try {
    let newPromise = fs.readFile(fileName, "utf-8", function (err, data) {
      if (err) throw err;
    });

    return newPromise;
  } catch (error) {
    return console.log(error);
  }
}

function writer(fileName, data) {
  try {
    let newPromise = fs.writeFile(fileName, data, function (err) {
      if (err) throw err;
    });

    return newPromise.then(() => fileName);
  } catch (error) {
    return console.log(error);
  }
}

function nameSaver(fileName) {
  try {
    let newPromise = fs.appendFile(
      "fileNames.txt",
      `${fileName}\n`,
      function (err) {
        if (err) throw err;
      }
    );

    return newPromise.then(() => fileName);
  } catch (error) {
    return console.log(error);
  }
}

function deleter(fileName) {
  try {
    let newPromise = fs.unlink(fileName, function (err) {
      if (err) throw err;
    });

    return newPromise
      .then(() => {
        console.log(fileName," file is deleted successfully.");
      })
      .catch((error) => {
        console.log(error);
      });
  } catch (error) {
    return console.log(error);
  }
}

module.exports = { reader, writer, nameSaver, deleter };
